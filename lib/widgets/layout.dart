import 'package:flutter/material.dart';

class MyLayout extends StatelessWidget {
  final Widget mychild;

  MyLayout(this.mychild);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white70,
      body: SafeArea(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.all(10),
              width: 100,
              height: 100,
              color: Colors.grey,
              child: Image(image: AssetImage('images/jpn.png'),),
            ),
            Container(
              child: mychild,
            ),
          ],
        ),
      ),
    );
  }
}
