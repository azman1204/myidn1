import 'package:flutter/material.dart';
import '../services/profile_service.dart';

class ProfileScreen extends StatefulWidget {

  ProfileScreen({Key? key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  var profile = {};

  @override
  void initState() {
    super.initState();
    profile['user_id'] = '';
    profile['name'] = '';
    profile['no_tel'] = '';
    profile['email'] = '';
    loading();
  }

  void loading() async {
    var profile2 = await ProfileService().getProfile();
    setState(() {
      profile = profile2;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Column(
        children: [
          CircleAvatar(
            radius: 50,
          ),
          myTextField(profile['user_id']),
          myTextField(profile['name']),
          myTextField(profile['no_tel']),
          myTextField(profile['email']),
        ],
      ),
    );
  }

  TextField myTextField(String placeholder) {
    return TextField(
      decoration: InputDecoration(hintText: placeholder),
    );
  }
}
