import 'package:flutter/material.dart';
import '../widgets/layout.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:path_provider/path_provider.dart';

class PhotoScreen extends StatefulWidget {
  const PhotoScreen({Key? key}) : super(key: key);

  @override
  _PhotoScreenState createState() => _PhotoScreenState();
}

class _PhotoScreenState extends State<PhotoScreen> {
  File? photo; // ? = boleh null

  @override
  Widget build(BuildContext context) {
    return MyLayout(
      Column(
        children: [
          Text(
            'PENGGUNA KALI PERTAMA',
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 30,
          ),
          FlatButton(
            onPressed: capturePhoto,
            child: Icon(
              Icons.add_a_photo,
              size: 40,
              color: Colors.green,
            ),
          ),
          Container(
            width: 400,
            margin: EdgeInsets.all(10),
            padding: EdgeInsets.all(10),
            color: Colors.grey,
            child: Column(
              children: [
                showPhoto(),
                ElevatedButton(onPressed: () {}, child: Text('Save'))
              ],
            ),
          ),
        ],
      ),
    );
  }

  capturePhoto() async {
    final ImagePicker _picker = ImagePicker();
    final XFile? photo2 = await _picker.pickImage(source: ImageSource.camera);
    var dir = await getApplicationDocumentsDirectory();
    String path = dir.path;
    print("PATH = $path");

    if (photo2 != null) {
      File file = File(photo2.path); // gambar dlm tmp dir
      String imgPath = path + "/photo.jpg";
      file.copy(imgPath);// copy ke doc. dir
      setState(() {
        photo = file;
      });
    }
  }

  showPhoto() {
    if (photo == null) {
      return Text('Tiada Photo...');
    } else {
      return CircleAvatar(radius: 50, backgroundImage: FileImage(photo!),);
    }
  }
}
