import 'package:flutter/material.dart';
import '../widgets/layout.dart';
import 'home_screen.dart';
import 'package:webview_flutter/webview_flutter.dart';
import '../services/login_servive.dart';
import '../util/global.dart' as global;

class LoginScreen extends StatelessWidget {
  String userId = '';
  String password = '';

  @override
  Widget build(BuildContext context) {
    return MyLayout(SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            color: Colors.white,
            margin: EdgeInsets.all(10),
            padding: EdgeInsets.all(10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                TextField(
                  decoration: InputDecoration(hintText: 'Username'),
                  onChanged: (String value) async {
                    userId = value;
                  }
                ),
                TextField(
                  decoration: InputDecoration(hintText: 'Password'),
                  obscureText: true,
                  onChanged: (String value) async {
                    password = value;
                  }
                ),
                Row(
                  children: [
                    Checkbox(
                      value: true,
                      onChanged: (val) {},
                    ),
                    Text('Remember Password'),
                    SizedBox(
                      width: 30,
                    ),
                    Text('Forgot Password')
                  ],
                ),
                ElevatedButton(
                    onPressed: () async {
                      bool success = await LoginService().authenticate(userId, password);
                      if (success) {
                        // set global variable
                        global.userId = userId;

                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return HomeScreen();
                        }));
                      } else {
                        // gagal. show dialog
                        createDialog(context);
                      }
                    },
                    child: Text('Login'))
              ],
            ),
          ),
          Container(
            height: 360,
            margin: EdgeInsets.fromLTRB(10,0, 10,0),
            child: WebView(
              initialUrl: 'http://192.168.0.165/myidn_api/index.php',
              javascriptMode: JavascriptMode.unrestricted,
            ),
          )
        ],
      ),
    ));
  }

  Future<void> createDialog(BuildContext context) {
    return showDialog(context: context, builder: (BuildContext context) {
      return AlertDialog(
        title: Text('PERHATIAN!'),
        content: Text('Kombinasi Id Pengguna dan Katalaluan tidak tepat'),
      );
    });
  }
}
