import 'package:flutter/material.dart';
import 'package:my_idn/screens/profile_screen.dart';
import '../util/global.dart' as global;

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          bottom: const TabBar(
            tabs: [
              Tab(icon: Icon(Icons.directions_car)),
              Tab(icon: Icon(Icons.directions_transit)),
              Tab(icon: Icon(Icons.directions_bike)),
            ],
          ),
          title: const Text('Tabs Demo'),
        ),
        body: TabBarView(
          children: [
            Center(child: Text('${global.userId}')),
            ProfileScreen(),
            Icon(Icons.directions_bike),
          ],
        ),
      ),
    );
  }
}
