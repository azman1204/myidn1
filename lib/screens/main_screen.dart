import 'package:flutter/material.dart';
import 'register_screen.dart';
import 'login_screen.dart';
import '../widgets/layout.dart';

class MainScreen extends StatelessWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MyLayout(
      Column(
        children: [
          Container(
            width: 400,
            height: 300,
            margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              color: Colors.white,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                SizedBox(
                  height: 20,
                ),
                Text(
                  'Welcome to My IDN Profile',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                ),
                SizedBox(
                  height: 5,
                ),
                Text('Lets find amazing features in this app'),
                SizedBox(
                  height: 50,
                ),
                ElevatedButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return LoginScreen();
                    }));
                  },
                  child: Text('Log Masuk'),
                ),
                ElevatedButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return RegisterScreen();
                    }));
                  },
                  child: Text('Pendaftaran'),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: 50,
                      height: 50,
                      color: Colors.grey,
                    ),
                    Container(
                      width: 50,
                      height: 50,
                      color: Colors.grey,
                    ),
                    Container(
                      width: 50,
                      height: 50,
                      color: Colors.grey,
                    ),
                    Container(
                      width: 50,
                      height: 50,
                      color: Colors.grey,
                    ),
                    Container(
                      width: 50,
                      height: 50,
                      color: Colors.grey,
                    ),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
