import 'package:flutter/material.dart';
import 'photo_screen.dart';
import '../widgets/layout.dart';
import '../services/registration_service.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  String nokp = '';
  String emel = '';
  String tel = '';
  String katalaluan = '';
  String pengesahan = '';

  @override
  Widget build(BuildContext context) {
    return MyLayout(
      Column(
        children: [
          Text(
            'PENGGUNA KALI PERTAMA',
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 30,
          ),
          Text('Isikan Maklumat Berikut'),
          Container(
            margin: EdgeInsets.all(10),
            padding: EdgeInsets.all(10),
            child: Column(
              children: [
                myTextField('No. Kad Pengenalan (9000091234)', 1),
                myTextField('Alamat emel (abc@def.com)', 2),
                myTextField('No. Tel Mudah Alih (0162370394)', 3),
                myTextField('Katalaluan', 4),
                myTextField('Pengesahan Katalaluan', 5),
                ElevatedButton(
                    onPressed: submitForm,
                    child: Text('TERUSKAN >>'))
              ],
            ),
          ),
        ],
      ),
    );
  }

  submitForm() async {
    // save data
    var registration = RegistrationService();
    String result = await registration.register(nokp, emel, tel, katalaluan, pengesahan);
    print('result $result');
    if (result == 'ok') {
      // go to next page (photo)
      Navigator.push(context,
          MaterialPageRoute(builder: (context) {
            return PhotoScreen();
          })
      );
    } else {
      // ada error. show alert msg
    }
  }

  TextField myTextField(String placeholder, int id) {
    return TextField(
      decoration: InputDecoration(hintText: placeholder),
      onChanged: (String val) {
        print(val);
        if (id == 1) {
          nokp = val;
        } else if (id == 2) {
          emel  =val;
        } else if (id == 3) {
          tel = val;
        } else if (id == 4) {
          katalaluan = val;
        } else if (id == 5) {
          pengesahan = val;
        }
      },
    );
  }
}
