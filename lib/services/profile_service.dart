import 'dart:convert';
import 'package:http/http.dart' as http;
import '../util/global.dart' as global;

class ProfileService {
  // return user berjaya login / gagal
  Future<dynamic> getProfile() async {
    print('profile service...');
    var url = Uri.parse('http://192.168.204.147/myidn_api/profile.php');
    var response = await http.post(url, body: {'user_id': global.userId});
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      var json = jsonDecode(response.body);
      return json;
    }
  }
}