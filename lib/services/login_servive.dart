import 'dart:convert';
import 'package:http/http.dart' as http;

class LoginService {
  // return user berjaya login / gagal
  Future<bool> authenticate(String userId, String password) async {
    print(userId + ' ' + password);
    var url = Uri.parse('http://192.168.0.165/myidn_api/login.php');
    var response = await http.post(url, body: {'user_id': userId, 'password': password});
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      var json = jsonDecode(response.body);
      String result = json['result'];

      if (result == 'passed') {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
}