import 'dart:convert';
import 'package:http/http.dart' as http;

class RegistrationService {
  // save a new registration
  Future<String> register(String nokp, String emel, String tel, String katalaluan, String pengesahan) async {
    print(nokp + ' ' + emel);
    var url = Uri.parse('http://192.168.0.165/myidn_api/registration.php');
    var response = await http.post(url, body: {'nokp': nokp, 'emel': emel, 'tel' : tel, 'katalaluan': katalaluan, 'pengesahan': pengesahan});
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      var json = jsonDecode(response.body);
      String result = json['result'];

      if (result == 'ok') {
        return 'ok';
      } else {
        return json['error'];
      }
    } else {
      return 'err';
    }
  }
}